button = {}
button._index = button

btnFont = love.graphics.setNewFont("fonts/KGHAPPY.ttf", 35)

local Vsync = true

Vsynctrue = true


function button.create(x, y, text, id, r, g, b)
	table.insert(button, {x = x, y = y, text = text, id = id, r = r, g = g, b = b, btnOver = false})
end

function button.draw()
	for i,v in ipairs(button) do
		love.graphics.print(v.text, v.x, v.y)
		if v.r == nil and v.g == nil and v.b == nil then
			v.r = 255
			v.g = 255
			v.b = 255
		end 
		if v.btnOver == true then
			v.x = 60
		elseif v.btnOver == false then
			v.x = 50
		end
	end
end

function button.click(x, y)
	for i,v in ipairs( button ) do
		if x > v.x and
		x < v.x + btnFont:getWidth(v.text) and
		y > v.y and
		y < v.y + btnFont:getHeight() then
			if v.id == "start" then
				gameState = "playing"
				for k=0, #button do
					table.remove(button)
				end
			elseif v.id == "inst" then
				gameState = "inst"
			elseif v.id == "opt" then
				gameState = "options"
				for k=0, #button do
					table.remove(button, k)
				end
				for k=0, #button do
					table.remove(button, k)
				end
				for k=0, #button do
					table.remove(button, k)
				end
				for k=0, #button do
					table.remove(button, k)
				end

				optionsLoad = true
				if optionsLoad then

					button.create(50, 360, "+", "vlmUp")
					button.create(100, 390, "-", "vlmDown")
					button.create(100, 420, "Vsync", "vsync")
					button.create(100, 500, "Back To Menu", "btm")
					
					optionsLoad = false
				end 
			elseif v.id == "vlmUp" then
				volume = volume + 0.1
				volumeText = volumeText + 1
				if volumeText > 10 and volume > 1.0 then
					volumeText = 10
					volume = 1
				end
			elseif v.id == "vlmDown" then
				volume = volume - 0.1
				volumeText = volumeText - 1
				if volumeText < 1 and volume < 0.1 then
					volume = 0
					volumeText = 0
				end
			elseif v.id == "vsync" then
				if Vsync == true then
					VsyncText = "Off"
					Vsync = false
				elseif Vsync == false then
					VsyncText = "On"
					Vsync = true
				end
			elseif v.id == "btm" then
				for k=0, #button do
					table.remove( button, k )
				end
				for k=0, #button do
					table.remove( button, k )
				end
				for k=0, #button do
					table.remove( button, k )
				end

				gameState = "menu"
				menuLoad = true

				if menuLoad then
					love.graphics.setBackgroundColor(100, 149, 237)
					button.create(50, 300, "START GAME", "start")
					button.create(50, 360, "HELP!", "inst")
					button.create(50, 420, "OPTIONS", "opt")
					button.create(50, 480, "QUIT",  "quit")
					cloud.create(math.random(200), 100, "res/cloud.png", 50, "right")
					cloud.create(200, 120, "res/cloud.png", 35, "left")

					menuLoad = false
				end
				
			elseif v.id == "quit" then
				love.event.push("quit")
			end
		end
	end
end

function button_check()
	for i,v in ipairs(button) do
		if MouseX > v.x and
		MouseX < v.x + btnFont:getWidth(v.text) and
		MouseY > v.y and
		MouseY < v.y + btnFont:getHeight() then
			v.btnOver = true
		else
			v.btnOver = false
		end
	end
end

function playerLoadAnim()
	playerHello = {}
	playerHello[1] = love.graphics.newImage("res/player/playerHello1.png")
	playerHello[2] = love.graphics.newImage("res/player/playerHello.png")
	playerHello[3] = love.graphics.newImage("res/player/playerHello2.png")
	playerHello.animtimer = 0
	playerHello.pic = playerHello[1]
	playerHello.pnum = 1
	playerHello.x = 200
	playerHello.y = 200
end

function playerUpdateAnim(dt)
	playerHello.pic = playerHello[playerHello.pnum]
	playerHello.animtimer = playerHello.animtimer + dt
	if playerHello.animtimer > 0.1 then
		playerHello.pnum = playerHello.pnum + 1
		playerHello.animtimer = 0
	end
	if playerHello.pnum == 3 then
		player.pnum = 1
	end
end

function playerDrawAnim()
	love.graphics.draw(playerHello.pic, playerHello.x, playerHello.y)
end
