function love.conf(t)
	t.title = "Bruno The Random"

	t.console = true

	t.screen.height = 800
	t.screen.width = 100

	t.screen.vsync = true

	if Vsync == true then
		t.screen.vsync = true
	elseif Vsync == false then
		t.screen.vsync = false
	end
end

