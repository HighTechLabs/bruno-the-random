love.filesystem.setIdentity("Bruno_files")

local menu = require("menu")
require("player")
require("cloud")

gameState = "menu"

menuLoad = true

local height = 800
local width = 100


function love.load()

	menuLoad = true
	optionsLoad = false

	showFPS = false

	volume = 0.5
	volumeText = 5
	VsyncText = "On"

	------ Loading libraries
	love.audio.setVolume(volume)

	if gameState == "menu" then
		menuLoad = true

		Textures = love.filesystem.createDirectory("Add-On/Textures")
		Modds = love.filesystem.createDirectory("Add-on/Mods")


		if menuLoad then
			love.graphics.setBackgroundColor(100, 149, 237)
			button.create(50, 300, "START GAME", "start")
			button.create(50, 360, "HELP!", "inst")
			button.create(50, 420, "OPTIONS", "opt")
			button.create(50, 480, "QUIT",  "quit")
			cloud.create(math.random(height), 100, "res/cloud.png", 50, "right")
			cloud.create(math.random(height), 120, "res/cloud.png", 35, "left")

			menuLoad = false
		end
		--playerLoadAnim()
	elseif gameState == "playing" then

	elseif gameState == "options" then
		if optionsLoad == true then
			
		end
	end

	--Other stuff
	player.checkModded()
	player.load()
	cloud.load()
end

function love.draw()
	if gameState == "menu" then
		button.draw()
		cloud.draw()
	elseif gameState == "options" then
		button.draw()
		love.graphics.print("Volume: ", 50, 500)
		love.graphics.print(volumeText, 50, 300)
		love.graphics.print(VsyncText, 50, 400)
	end

	if showFPS == true then
		fpsText = love.graphics.print(FPS, 100, 100)
	elseif showFPS == false then
		fpsText = love.graphics.print("", 100, 100)
	end
end

function love.mousepressed(x, y)
	button.click(x, y)
end

function love.keypressed(key)
	if key == "f12" then
		showFPS = true
	elseif key == "f12" and showFPS == true then 
		showFPS = false
	end
end

function love.update(dt)
	MouseX = love.mouse.getX()
	MouseY = love.mouse.getY()

	FPS = love.timer.getFPS()

	if gameState == "menu" then
		button_check()
		cloud.update(dt)
		--playerUpdateAnim(dt)
	end

	--player.checkModded(dt)

end