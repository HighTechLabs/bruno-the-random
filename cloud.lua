cloud = {} 

function cloud.create(x, y, cloudImg, cloudSpeed, dir)
	table.insert(cloud, {x = x, y = y, cloudImg = cloudImg, cloudSpeed = cloudSpeed, dir = dir})
end

function cloud.load()
	for i,v in ipairs(cloud) do
		CloudImage = love.graphics.newImage(v.cloudImg)
	end
end

function cloud.draw()
	for i,v in ipairs(cloud) do
		love.graphics.draw(CloudImage, v.x, v.y)
	end
end

function cloud.update(dt)
	for i,v in ipairs(cloud) do
		if v.x > 800 and v.dir == "right" then
			v.x = -200
		elseif v.x < -182 and v.dir == "left" then
			v.x = 800
		end
		if v.dir == "right" then
			v.x = v.x + v.cloudSpeed * dt
		elseif v.dir == "left" then
			v.x = v.x - v.cloudSpeed * dt
		end
	end
end