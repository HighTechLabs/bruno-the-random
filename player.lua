player = {}


function player.load()
	player.x = 500
	player.y = 100
	player.speed = 250
	player.friction = 0.5
	playerR = {}
	playerR[1] = love.graphics.newImage("res/player/playerR1.png")
	--playerR[2] = love.graphics.newImage("res/player/playerR2.png")
	playerL = {}
	player.pnum = 1
	player.animtimer = 0
	player.pic = playerR[1]

end

function player.checkModded()
	player.ModdedR1 = love.filesystem.exists("Add-on/Textures/brunoR1.png")
	player.ModdedR2 = love.filesystem.exists("Add-on/Textures/brunoR2.png")
	player.ModdedR3 = love.filesystem.exists("Add-on/Textures/brunoR3.png")
	player.ModdedR4 = love.filesystem.exists("Add-on/Textures/brunoR4.png")
	player.ModdedL1 = love.filesystem.exists("Add-on/Textures/brunoL1.png")
	player.ModdedL2 = love.filesystem.exists("Add-on/Textures/brunoL2.png")
	player.ModdedL3 = love.filesystem.exists("Add-on/Textures/brunoL3.png")
	player.ModdedL4 = love.filesystem.exists("Add-on/Textures/brunoL4.png")

	if player.ModdedR1 and player.ModdedR2 and
	player.ModdedR3 and player.ModdedR4 and
	player.ModdedL1 and player.ModdedL2 and
	player.ModdedL3 and player.ModdedL4 then

	else
		print( "not found" )
	end
end

function player.draw()

end

function player.control(dt)
	if love.keyboard.isDown("d") then
		player.x = player.x + player.speed * dt
		player.animtimer = player.animtimer + dt
		player.pnum = player.pnum + 1
	elseif love.keyboard.isDown("a") then
		player.x = player.x - player.speed * dt
	end

end